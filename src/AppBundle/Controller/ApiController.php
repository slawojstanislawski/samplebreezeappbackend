<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{

    public function apiAction(Request $request)
    {
        /** @var \Adrotec\BreezeJs\Framework\Application $api */
        $app = $this->container->get('adrotec_webapi');


        $app->addResources(array(
            'Users' => 'AppBundle\Entity\User',
            'Departments' => 'AppBundle\Entity\Department',
            'Jobs' => 'AppBundle\Entity\Job',
            'Addresses' => 'AppBundle\Entity\Address',
        ));

        // $request->attributes->set($request->attributes->get('resource'));

        $response = $app->handle($request);
        return $response;
    }

}