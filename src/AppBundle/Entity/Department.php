<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/** 
* @ORM\Entity
* @ORM\Table(name="departments")
*/

class Department {

	/** 
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="IDENTITY")
	* @ORM\Column(type="integer", name="id")
	*/
	protected $id;

	/** 
	* @ORM\Column(type="string", name="description")
	*/
	protected $description;

	/** 
	* @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="department")
	*/
	protected $users;

	/** 
	* @ORM\Column(type="string", name="name")
	*/
	protected $name;

	public function __construct() {
		$this->users = new ArrayCollection();
	}

	public function setId($id = null) {
		$this->id = $id;
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	public function setDescription($description = null) {
		$this->description = $description;
		return $this;
	}

	public function getDescription() {
		return $this->description;
	}

	public function addToUsers(\AppBundle\Entity\User $singleEntity) {
		if(!$this->getUsers()->contains($singleEntity)) {
			$this->users->add($singleEntity);
			$singleEntity->setDepartment($this);
		}
		return $this;
	}

	public function addUsers(Collection $users) {
		foreach($users as $singleEntity) {
			$this->addToUsers($singleEntity);
		}
		return $this;
	}

	public function removeFromUsers(\AppBundle\Entity\User $singleEntity) {
		if($this->getUsers()->contains($singleEntity)) {
			$this->users->removeElement($singleEntity);
			$singleEntity->setDepartment(null);
		}
		return $this;
	}

	public function removeUsers(Collection $users) {
		foreach($users as $singleEntity) {
			$this->removeFromUsers($singleEntity);
		}
		return $this;
	}

	public function clearUsers() {
		foreach($this->users as $singleEntity) {
			$this->removeFromUsers($singleEntity);
		}
		return $this;
	}

	public function getUsers() {
		return $this->users;
	}

	public function setUsers($users) {
		$this->clearUsers();
		foreach($users as $singleEntity) {
			$this->addToUsers($singleEntity);
		}
		return $this;
	}

	public function setName($name = null) {
		$this->name = $name;
		return $this;
	}

	public function getName() {
		return $this->name;
	}

}

