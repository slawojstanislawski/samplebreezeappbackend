<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/** 
* @ORM\Entity
* @ORM\Table(name="users")
*/

class User {

	/** 
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="IDENTITY")
	* @ORM\Column(type="integer", name="id")
	*/
	protected $id;

	/**
	 * @ORM\OneToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist", "merge", "remove"})
	 * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
	 */
	protected $address;

	/**
	 * @ORM\Column(type="integer", name="address_id", nullable=true)
	 */
	protected $addressId;

	/** 
	* @ORM\Column(type="text", name="bio")
	*/
	protected $bio;

	/** 
	* @ORM\Column(type="string", name="cell_phone")
	*/
	protected $cellPhone;

	/** 
	* @ORM\Column(type="date", name="date_of_birth")
	*/
	protected $dateOfBirth;

	/** 
	* @ORM\ManyToOne(targetEntity="AppBundle\Entity\Department", inversedBy="users")
	*/
	protected $department;

	/** 
	* @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="manager")
	*/
	protected $directReports;

	/** 
	* @ORM\Column(type="string", name="email")
	*/
	protected $email;

	/** 
	* @ORM\Column(type="string", name="first")
	*/
	protected $firstName;

	/** 
	* @ORM\ManyToOne(targetEntity="AppBundle\Entity\Job")
	* @ORM\JoinColumn(name="job_id", referencedColumnName="id")
	*/
	protected $job;

	/**
	 * @ORM\Column(type="integer", name="job_id", nullable=true)
	 */
	protected $jobId;

	/** 
	* @ORM\Column(type="string", name="last_name")
	*/
	protected $lastName;

	/** 
	* @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="directReports")
	* @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
	*/
	protected $manager;

	/** 
	* @ORM\Column(type="string", name="office_phone")
	*/
	protected $officePhone;

	/** 
	* @ORM\Column(type="string", name="profile_pic", nullable=true)
	*/
	protected $profilePic;

	/** 
	* @ORM\Column(type="string", name="twitter")
	*/
	protected $twitter;

	/** 
	* @ORM\Column(type="string", name="website")
	*/
	protected $website;

	public function __construct() {
		$this->directReports = new ArrayCollection();
	}

	public function setId($id = null) {
		$this->id = $id;
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	public function setAddress(\AppBundle\Entity\Address $address = null) {
		$this->address = $address;
		return $this;
	}

	public function getAddress() {
		return $this->address;
	}

	public function setBio($bio = null) {
		$this->bio = $bio;
		return $this;
	}

	public function getBio() {
		return $this->bio;
	}

	public function setCellPhone($cellPhone = null) {
		$this->cellPhone = $cellPhone;
		return $this;
	}

	public function getCellPhone() {
		return $this->cellPhone;
	}

	public function setDateOfBirth($dateOfBirth = null) {
		$this->dateOfBirth = $dateOfBirth;
		return $this;
	}

	public function getDateOfBirth() {
		return $this->dateOfBirth;
	}

	public function setDepartment(\AppBundle\Entity\Department $department = null) {
		$this->department = $department;
		($this->department) ? $this->department->addToUsers($this) : $this->department->removeFromUsers($this);
		return $this;
	}

	public function getDepartment() {
		return $this->department;
	}

	public function addToDirectReports(\AppBundle\Entity\User $singleEntity) {
		if(!$this->getDirectReports()->contains($singleEntity)) {
			$this->directReports->add($singleEntity);
			$singleEntity->setManager($this);
		}
		return $this;
	}

	public function addDirectReports(Collection $directReports) {
		foreach($directReports as $singleEntity) {
			$this->addToDirectReports($singleEntity);
		}
		return $this;
	}

	public function removeFromDirectReports(\AppBundle\Entity\User $singleEntity) {
		if($this->getDirectReports()->contains($singleEntity)) {
			$this->directReports->removeElement($singleEntity);
			$singleEntity->setManager(null);
		}
		return $this;
	}

	public function removeDirectReports(Collection $directReports) {
		foreach($directReports as $singleEntity) {
			$this->removeFromDirectReports($singleEntity);
		}
		return $this;
	}

	public function clearDirectReports() {
		foreach($this->directReports as $singleEntity) {
			$this->removeFromDirectReports($singleEntity);
		}
		return $this;
	}

	public function getDirectReports() {
		return $this->directReports;
	}

	public function setDirectReports($directReports) {
		$this->clearDirectReports();
		foreach($directReports as $singleEntity) {
			$this->addToDirectReports($singleEntity);
		}
		return $this;
	}

	public function setEmail($email = null) {
		$this->email = $email;
		return $this;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setFirstName($firstName = null) {
		$this->firstName = $firstName;
		return $this;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setJob(\AppBundle\Entity\Job $job = null) {
		$this->job = $job;
		return $this;
	}

	public function getJob() {
		return $this->job;
	}

	public function setLastName($lastName = null) {
		$this->lastName = $lastName;
		return $this;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function setManager(\AppBundle\Entity\User $manager = null) {
		$this->manager = $manager;
		return $this;
	}

	public function getManager() {
		return $this->manager;
	}

	public function setOfficePhone($officePhone = null) {
		$this->officePhone = $officePhone;
		return $this;
	}

	public function getOfficePhone() {
		return $this->officePhone;
	}

	public function setProfilePic($profilePic = null) {
		$this->profilePic = $profilePic;
		return $this;
	}

	public function getProfilePic() {
		return $this->profilePic;
	}

	public function setTwitter($twitter = null) {
		$this->twitter = $twitter;
		return $this;
	}

	public function getTwitter() {
		return $this->twitter;
	}

	public function setWebsite($website = null) {
		$this->website = $website;
		return $this;
	}

	public function getWebsite() {
		return $this->website;
	}

}

