<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
* @ORM\Entity
* @ORM\Table(name="jobs")
*/

class Job {

	/** 
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="IDENTITY")
	* @ORM\Column(type="integer", name="id")
	*/
	protected $id;

	/** 
	* @ORM\Column(type="string", name="description")
	*/
	protected $description;

	/** 
	* @ORM\Column(type="string", name="title")
	*/
	protected $title;

	public function setId($id = null) {
		$this->id = $id;
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	public function setDescription($description = null) {
		$this->description = $description;
		return $this;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setTitle($title = null) {
		$this->title = $title;
		return $this;
	}

	public function getTitle() {
		return $this->title;
	}

}

