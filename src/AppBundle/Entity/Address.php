<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
* @ORM\Entity
* @ORM\Table(name="addresses")
*/

class Address {

	/** 
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="IDENTITY")
	* @ORM\Column(type="integer", name="id")
	*/
	protected $id;

	/** 
	* @ORM\Column(type="string", name="postal")
	*/
	protected $postal;

	/** 
	* @ORM\Column(type="string", name="street")
	*/
	protected $street;

	public function setId($id = null) {
		$this->id = $id;
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	public function setPostal($postal = null) {
		$this->postal = $postal;
		return $this;
	}

	public function getPostal() {
		return $this->postal;
	}

	public function setStreet($street = null) {
		$this->street = $street;
		return $this;
	}

	public function getStreet() {
		return $this->street;
	}

}

